"""
    pypsyc.server.root
    ~~~~~~~~~~~~~~~~~~

    :copyright: 2010 by Manuel Jacob
    :license: MIT
"""


class Root(object):
    def __init__(self, entity):
        self.entity = entity
        self.server = entity.server
