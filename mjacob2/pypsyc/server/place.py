"""
    pypsyc.server.place
    ~~~~~~~~~~~~~~~~~~

    :copyright: 2010 by Manuel Jacob
    :license: MIT
"""
from pypsyc.protocol import (ContextMaster as ContextProtocol,
                             ConferencingServer as ConferencingProtocol)


class Place(object):
    def __init__(self, entity):
        self.entity = entity
        entity.add_handler(ContextProtocol(self))
        self.conferencing_protocol = ConferencingProtocol(self)
        entity.add_handler(self.conferencing_protocol)
        self.members = set()

    def enter_request(self, uni):
        nick = uni.rsplit('/', 1)[1].lstrip('~')
        self.conferencing_protocol.cast_member_entered(uni, nick)
        self.members.add(uni)
        return self.entity.context_master.add_member(uni)

    def leave_context(self, uni):
        self.entity.context_master.remove_member(uni)
        self.conferencing_protocol.cast_member_left(uni)
        self.members.remove(uni)

    def public_message(self, source, message):
        member = source.rsplit('/', 1)[0]
        if member in self.members:
            self.conferencing_protocol.cast_public_message(member, message)
