from setuptools import setup, find_packages

import pypsyc

setup(
    name = "pypsyc",
    version = pypsyc.__version__,
    packages = find_packages(exclude='tests'),
    package_data = {
        'pypsyc.client': ['psyc.ico'],
        'pypsyc.server.webif': ['templates/*']
    },
    zip_safe = False,
    scripts = ['bin/pypsyc', 'bin/pypsycd'],
    entry_points = '''
        [pypsyc.server.packages]
        root = pypsyc.server.root:Root
        person = pypsyc.server.person:Person
        place = pypsyc.server.place:Place
    '''
)
