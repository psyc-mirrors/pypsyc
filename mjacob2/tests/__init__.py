import tests.helpers
from twisted.python.log import defaultObserver, PythonLoggingObserver
defaultObserver.stop()
PythonLoggingObserver().start()
