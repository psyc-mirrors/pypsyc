"""
    :copyright: 2010 by Manuel Jacob
    :license: MIT
"""


def _callback(*args, **kwds): # pragma: no cover
    print ('callback', args, kwds)

def _test_accounts_view(): # pragma: no cover
    accounts_view = AccountsView()
    Controller(controller.AccountsController, accounts_view)
    accounts_view.window.connect('destroy', lambda w: gtk.main_quit())
    for i in xrange(10):
        accounts_view.accounts.append(("psyc://server/~account%i" % i, True))
    accounts_view.accounts[4] = ("psyc://server/~updatedaccount4", False)
    #return

    from pypsyc.client.model import Account
    account = Account(None, "", "", "", False, False)
    accounts_view.show_addedit_dialog(account.__dict__, True, _callback)

def _test_tabs_view(): # pragma: no cover
    status_icon = gtk.StatusIcon()
    status_icon.set_from_file('pypsyc/client/psyc.ico')
    status_icon.connect('activate', lambda w: tabs_view.on_status_icon_click())
    tabs_view = TabsView(status_icon)
    tabs_view.window.connect('destroy', lambda w: gtk.main_quit())
    Controller(controller.TabsController, tabs_view)

    conversation_view = tabs_view.show_conversation("Conversation 1")
    Controller(controller.ConversationController, conversation_view)
    tabs_view.focus_tab(conversation_view)

    conference_view = tabs_view.show_conference("Conversation 2")
    Controller(controller.ConferenceController, conference_view)

    def show():
        for i in xrange(40):
            conversation_view.show_message("Message %02i" % i)
        for i in xrange(40):
            conference_view.members.append(("psyc://server/~person%02i" % i,
                                            "person%02i" % i))
            conference_view.show_message("Message %02i" % i)
        conference_view.members[20] = ("psyc://server/~longpersonname20",
                                       "longpersonname20")
    idle_add(show)

def _test_dump_view(): # pragma: no cover
    dump_view = DumpView()
    Controller(controller.DumpController, dump_view)
    dump_view.show_line('o', 'a', 'psyc://server/~account')
    dump_view.show_line('i', ':_tag_relay\t\xc8\xe6', 'psyc://server/~account')

def _test_main_view(): # pragma: no cover
    main_view = MainView()
    Controller(controller.MainController, main_view)
    Controller(controller.FriendListController, main_view.friends_view)
    for i in xrange(40):
        main_view.friends_view.friends.append(("Friend %02i" % i, False,
                                               'pending'))
    for i in xrange(0, 40, 2):
        main_view.friends_view.friends[i] = ("Friend %02i updated" % i, False,
                                             'offered')
    for i in xrange(0, 40, 3):
        main_view.friends_view.friends[i] = ("Friend %02i updated 2" % i, True,
                                             'established')
    #return

    account_dict = {'password': '', 'save_password': False}
    main_view.show_password_dialog("psyc://server/~account", account_dict,
                                   _callback)
    main_view.show_no_such_user("psyc://server/~account")
    main_view.show_auth_error("psyc://server/~account", "Error description")
    main_view.show_open_conv_dialog(["psyc://server/~person1",
                                     "psyc://server/~person2"], _callback)
    main_view.show_open_conf_dialog(["psyc://server/~person1",
                                     "psyc://server/~person2"], _callback)
    main_view.show_add_friend_dialog(["psyc://server/~person1",
                                      "psyc://server/~person2"], _callback)


if __name__ == '__main__': # pragma: no cover
    class Controller(object):
        def __init__(self, controller, view):
            self.controller = controller
            self.c_name = controller.__name__
            view.controller = self
        def __getattr__(self, attr):
            m = getattr(self.controller, attr, None)
            assert m, "%s has no method %s" % (self.c_name, attr)
            def f(*args, **kwds):
                m_argcount = m.__code__.co_argcount - 1 # minus self
                f_argcount = len(args) + len(kwds)
                assert m_argcount == f_argcount, \
                       "%s.%s has %s arguments, called with %s arguments" % (
                       self.c_name, attr, m_argcount, f_argcount)
                print "%s.%s called: %s, %s" % (self.c_name, attr, args, kwds)
            return f

    from gobject import idle_add
    import gtk

    from pypsyc.client.view import AccountsView, TabsView, DumpView, MainView
    from pypsyc.client import controller

#    _test_accounts_view()
#    _test_tabs_view()
#    _test_dump_view()
#    _test_main_view()
    gtk.main()
