"""
    :copyright: 2010 by Manuel Jacob
    :license: MIT
"""
from mock import Mock
from nose.tools import assert_raises

from pypsyc.client.observable import ObsAttr, ObsObj, ObsList, ObsDict


class StubObsObj(ObsObj):
    foo = ObsAttr('foo')


def test_obs_obj():
    obs_obj = StubObsObj()
    update_evt = Mock()
    obs_obj.update_evt['foo'] += update_evt

    obs_obj.ignore = 'foo'
    assert not hasattr(obs_obj, 'foo')
    obs_obj.foo = 'foo'
    obs_obj.foo = 'foo'
    obs_obj.foo = 'bar'
    assert obs_obj.foo == 'bar'

    del obs_obj.foo
    assert not hasattr(obs_obj, 'foo')
    assert_raises(AttributeError, obs_obj.__delattr__, 'foo')

    assert update_evt.call_args_list == [
            ((None, 'foo'),), (('foo', 'bar'),), (('bar', None),)]


class TestObsList(object):
    def test_basic(self):
        obs_list = ObsList(xrange(10))
        setitem_evt = Mock()
        obs_list.setitem_evt += setitem_evt
        delitem_evt = Mock()
        obs_list.delitem_evt += delitem_evt
        insert_evt = Mock()
        obs_list.insert_evt += insert_evt

        obs_list[3] = 3
        obs_list[3] = 333
        del obs_list[3]
        obs_list.insert(3, 3)

        assert obs_list == list(xrange(10))
        assert setitem_evt.call_args_list == [((3, 3, 333),)]
        assert delitem_evt.call_args_list == [((3, 333),)]
        assert insert_evt.call_args_list == [((3, 3),)]

    def test_ext_add(self):
        obs_list = ObsList()
        insert_evt = Mock()
        obs_list.insert_evt += insert_evt

        obs_list.append(42)
        obs_list.extend([42, 1337])
        obs_list += [42, 1337]

        assert obs_list == [42, 42, 1337, 42, 1337]
        assert insert_evt.call_args_list == [
                ((0, 42),), ((1, 42),), ((2, 1337),), ((3, 42),), ((4, 1337),)]

    def test_ext_del(self):
        obs_list = ObsList(xrange(10))
        delitem_evt = Mock()
        obs_list.delitem_evt += delitem_evt

        assert obs_list.pop() == 9
        assert obs_list.pop(8) == 8
        obs_list.remove(7)

        assert delitem_evt.call_args_list == [((9, 9),), ((8, 8),), ((7, 7),)]
        assert obs_list == list(xrange(7))

    def test_reverse(self):
        obs_list = ObsList(xrange(10))
        obs_list.reverse()
        assert obs_list == list(reversed(xrange(10)))

    def test_updated_item(self):
        obs_list = ObsList(['a', 'b'])
        update_evt = Mock()
        obs_list.update_evt += update_evt

        obs_list.updated_item('b')
        assert update_evt.call_args_list == [((1, 'b'),)]


class TestObsDict(object):
    def test_basic(self):
        obs_dict = ObsDict(a='A')
        setitem_evt = Mock()
        obs_dict.setitem_evt += setitem_evt
        delitem_evt = Mock()
        obs_dict.delitem_evt += delitem_evt

        obs_dict['a'] = 'A'
        obs_dict['a'] = 'X'
        del obs_dict['a']
        obs_dict['b'] = 'B'

        assert obs_dict == dict(b='B')
        assert setitem_evt.call_args_list == [(('a', 'A', 'X'),),
                                              (('b', None, 'B'),)]
        assert delitem_evt.call_args_list == [(('a', 'X'),)]

    def test_pop(self):
        obs_dict = ObsDict(a='A', b='B')
        delitem_evt = Mock()
        obs_dict.delitem_evt += delitem_evt

        assert obs_dict.pop('a') == 'A'
        assert obs_dict.pop('a', 'X') == 'X'
        assert obs_dict.pop('b', 'X') == 'B'

        assert obs_dict == {}
        assert delitem_evt.call_args_list == [(('a', 'A'),), (('b', 'B'),)]

    def test_popitem(self):
        obs_dict = ObsDict(a='A', b='B')
        delitem_evt = Mock()
        obs_dict.delitem_evt += delitem_evt

        k, v = obs_dict.popitem()
        assert v == k.upper()

        assert len(obs_dict) == 1
        assert delitem_evt.call_args_list == [((k, v),)]

    def test_clear(self):
        obs_dict = ObsDict(a='A', b='B')
        delitem_evt = Mock()
        obs_dict.delitem_evt += delitem_evt

        obs_dict.clear()

        assert delitem_evt.call_args_list == [(('a', 'A'),), (('b', 'B'),)]

    def test_update(self):
        obs_dict = ObsDict()
        setitem_evt = Mock()
        obs_dict.setitem_evt += setitem_evt

        obs_dict.update({'a': 'A'}, b='B')
        obs_dict.update((('c', 'C'),))

        assert obs_dict == dict(a='A', b='B', c='C')
        assert setitem_evt.call_args_list == [
                (('a', None, 'A'),), (('b', None, 'B'),), (('c', None, 'C'),)]

    def test_setdefault(self):
        obs_dict = ObsDict(a='A')
        setitem_evt = Mock()
        obs_dict.setitem_evt += setitem_evt

        assert obs_dict.setdefault('a') == 'A'
        assert obs_dict.setdefault('b', 'B') == 'B'

        assert obs_dict == dict(a='A', b='B')
        assert setitem_evt.call_args_list == [(('b', None, 'B'),)]

    def test_updated_item(self):
        obs_dict = ObsDict({'a': 'A', 'b': 'B'})
        update_evt = Mock()
        obs_dict.update_evt += update_evt

        obs_dict.updated_item('b')
        assert update_evt.call_args_list == [(('b', 'B'),)]
