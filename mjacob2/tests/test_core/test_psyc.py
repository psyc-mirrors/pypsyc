"""
    :copyright: 2010 by Manuel Jacob
    :license: MIT
"""
from mock import Mock
from nose.tools import assert_raises
from tests.constants import USER1_UNI, USER2_UNI
from tests.helpers import check_success, rendered

from pypsyc.core.psyc import PSYCPacket, PSYCObject
from pypsyc.util import schedule


class TestPSYCPacket(object):
    def test_vars(self):
        packet = PSYCPacket(
            modifiers={
                ':': {'_foo': 'bar'},
                '=': {'_spam': 'eggs'}
            }
        )
        self._test(packet, [':_foo\tbar', '=_spam\teggs'])

    def test_method(self):
        packet = PSYCPacket(mc='_notice_foo')
        self._test(packet, ['_notice_foo'])

    def test_packet(self):
        packet = PSYCPacket(
            modifiers={
                ':': {'_foo': 'bar'}
            },
            mc='_message_foo',
            data='this is\ndata'
        )
        self._test(packet, [':_foo\tbar', '_message_foo', 'this is\ndata'])

    def test_binary1(self):
        packet = PSYCPacket({':': {'_foo': 'foo\nbar'}})
        self._test(packet, [':_foo 7\tfoo\nbar'])
        assert PSYCPacket.parse([':_foo 7\tfoo', 'bar']) == packet

    def test_binary2(self):
        packet = PSYCPacket({':': {'_foo': 'foo\n\nbar'}})
        self._test(packet, [':_foo 8\tfoo\n\nbar'])
        assert PSYCPacket.parse([':_foo 8\tfoo', '', 'bar']) == packet

    def _test(self, packet, rendered_):
        assert PSYCPacket.parse(packet.render()) == packet
        assert rendered(packet) == rendered_

    def test_nomethod(self):
        packet = PSYCPacket(data='foobar')
        assert_raises(AssertionError, list, packet.render())

    def test_repr(self):
        s = '\n'.join((
            r"PSYCPacket(",
            r"    modifiers={",
            r"        ':': {'_foo': 'bar'},",
            r"        '=': {'_foo': 'bar'}",
            r"    },",
            r"    mc='_message_foo',",
            r"    data='this is\ndata'",
            r")"
        ))
        assert repr(eval(s)) == s

    def test_equal(self):
        assert PSYCPacket() == PSYCPacket()
        assert not PSYCPacket() != PSYCPacket()

    def test_empty(self):
        packet = PSYCPacket()
        self._test(packet, [])
        assert packet.modifiers == {':': {}}
        assert packet.mc is None
        assert packet.data == ''

    def test_from_kwds(self):
        packet1 = PSYCPacket.from_kwds(mc='_message_public',
                                       data="hello", _somevar='foo')
        packet2 = PSYCPacket(mc='_message_public', data="hello",
                             modifiers={':': {'_somevar': 'foo'}})
        assert packet1 == packet2


class TestPSYCObject(object):
    def test_handle_packet(self):
        psyc = PSYCObject(None)
        psyc.handlers['_message'] = message = Mock()
        psyc.handlers['_message_public'] = message_public = Mock()

        p1 = PSYCPacket() # psyc_packet should return
        p2 = PSYCPacket(mc='_message_public')
        p3 = PSYCPacket(mc='_message_private')

        assert psyc.handle_packet({}, p1.render()) == p1
        assert psyc.handle_packet({}, p2.render()) == p2
        assert psyc.handle_packet({}, p3.render()) == p3
        assert message_public.call_args_list == [((p2,),)]
        assert message.call_args_list == [((p3,),)]

    def test_handle_packet_internal_error(self):
        psyc = PSYCObject(None)
        psyc.handlers['_message_public'] = Mock(side_effect=Exception)

        packet = PSYCPacket(mc='_message_public')
        assert psyc.handle_packet({}, packet.render()) == packet

    def test_sendmsg_packet(self):
        packet = PSYCPacket()
        sendfunc = Mock()
        psyc = PSYCObject(sendfunc, USER1_UNI)

        psyc.sendmsg(USER2_UNI, packet)
        header = {'_source': USER1_UNI, '_target': USER2_UNI}
        assert sendfunc.call_args_list == [((header, rendered(packet)),)]

    def test_sendmsg_kwds(self):
        packet = PSYCPacket({':': {'_somevar': 'foo'}}, mc='_message_private')
        sendfunc = Mock()
        psyc = PSYCObject(sendfunc, USER1_UNI)

        psyc.sendmsg(USER2_UNI, mc='_message_private', _somevar='foo')
        header = {'_source': USER1_UNI, '_target': USER2_UNI}
        assert sendfunc.call_args_list == [((header, rendered(packet)),)]

    @check_success
    def test_async(self):
        MC = '_request_foo'
        ECHO = PSYCPacket(mc='_echo_foo')
        o1 = PSYCObject(lambda *args: o2.handle_packet(*args), USER1_UNI)
        o2 = PSYCObject(lambda *args: o1.handle_packet(*args), USER2_UNI)
        o2.handlers[MC] = lambda _: ECHO

        def _test():
            ret = o1.sendmsg(USER2_UNI, mc=MC)
            assert ret == ECHO
            self.success = True
        schedule(_test)

    def test_async_internal_error(self):
        MC = '_request_foo'
        ERROR = PSYCPacket(mc='_error_internal')
        o1 = PSYCObject(lambda *args: o2.handle_packet(*args), USER1_UNI)
        o2 = PSYCObject(lambda *args: o1.handle_packet(*args), USER2_UNI)
        o2.handlers[MC] = Mock(side_effect=Exception)

        ret = o1.sendmsg(USER2_UNI, mc=MC)
        assert ret == ERROR
