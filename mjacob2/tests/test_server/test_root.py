"""
    :copyright: 2010 by Manuel Jacob
    :license: MIT
"""
from mock import Mock
from tests.constants import SERVER1

from pypsyc.server import Entity
from pypsyc.server.root import Root


class TestRoot(object):
    def setup(self):
        server = Mock()
        server.hostname = SERVER1

        root_entity = Entity(server=server)
        self.root = Root(root_entity)

    def test_root(self):
        pass
