"""
    :copyright: 2010 by Manuel Jacob
    :license: MIT
"""
from greenlet import greenlet
from nose.tools import assert_raises
from tests.helpers import mockify, mockified, check_success

from pypsyc.server.db import Database


class TestDatabase:
    def test_sync(self):
        db = Database(':memory:')
        db.execute('CREATE TABLE test(test TEXT)')
        db.execute("INSERT INTO test VALUES ('test')")

        result = db.fetch('SELECT test FROM test')
        assert result == [('test',)]
        assert isinstance(result[0][0], str)

        db.stop()

    @check_success
    @mockified('pypsyc.server.db',['sqlite3', 'Thread', 'reactor'])
    def test_async(self, sqlite3, Thread, reactor):
        db = Database('')
        mockify(db, ['_execute', '_fetch'])
        e = Exception()
        db._execute.side_effect = e

        def f():
            assert_raises(Exception, db.execute)
            assert db.fetch() == db._fetch.return_value
            self.success = True
        gl = greenlet(f)
        gl.switch()

        # emulate reactor calling this methods
        gl.throw(e)
        gl.switch(db._fetch.return_value)

        db.stop()
        assert db.thread.method_calls == [('start',), ('join',)]

        db.run_async()
        assert sqlite3.method_calls == [('connect', ('',))]
        assert reactor.method_calls == [
                ('callFromThread', (gl.throw, e)),
                ('callFromThread', (gl.switch, db._fetch.return_value))]
