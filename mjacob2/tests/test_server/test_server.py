"""
    :copyright: 2010 by Manuel Jacob
    :license: MIT
"""
from contextlib import contextmanager

from mock import Mock
from nose.tools import assert_raises
from tests.constants import (SERVER1, SERVER1_UNI, USER1, USER1_UNI, PLACE,
     INTERFACE, PORT, PASSWORD)
from tests.helpers import mockified, rendered, AsyncMethod

from pypsyc.core.psyc import PSYCPacket
from pypsyc.server import Entity, Server
from pypsyc.server.routing import _TreeNode


class TestEntity(object):
    def setup(self):
        self.server = Mock()
        self.server.hostname = SERVER1
        self.entity = Entity(server=self.server)

    def test_uni(self):
        assert self.entity.uni == SERVER1_UNI
        assert Entity(self.entity, USER1).uni == USER1_UNI

    def test_castmsg(self):
        HEADER = {'_context': self.entity.uni}
        PACKET = PSYCPacket(mc='_message_public', data="Hello")

        self.entity.castmsg(PACKET)
        assert self.server.routing.method_calls == [
                ('route_multicast', (HEADER, rendered(PACKET)))]

    def test_castmsg_kwds(self):
        HEADER = {'_context': self.entity.uni}
        PACKET = PSYCPacket(mc='_message_public', data="Hello")

        self.entity.castmsg(mc='_message_public', data="Hello")
        assert self.server.routing.method_calls == [
                ('route_multicast', (HEADER, rendered(PACKET)))]


def StubEntity(*args, **kwds):
    entity = _TreeNode(*args, **kwds)
    entity.packages = {}
    return entity

class TestServer(object):
    @mockified('pypsyc.server', ['Routing', 'Entity', 'run_webif', 'signal'])
    def test_server1(self, Routing, Entity, run_webif, signal):
        WEBIF_PORT = 8080
        routing = Routing.return_value
        root = Entity.return_value = StubEntity()
        run_webif.side_effect = AsyncMethod()

        server = Server(SERVER1, INTERFACE, PORT, WEBIF_PORT, ':memory:')
        assert Routing.call_args_list == [((SERVER1, INTERFACE),)]
        assert Entity.call_args_list == [({'server': server},)]
        assert routing.method_calls == [('init', (root,)), ('listen', (PORT,))]
        assert run_webif.call_args_list == [
                ((server, INTERFACE, WEBIF_PORT, None),)]
        assert signal.signal.called

        with mockified('pypsyc.server', ['iter_entry_points', 'Entity']) as x:
            with self._test_load_package(root, *x) as packages:
                assert server.add_package(None, 'package') == packages[0]
                assert server.add_place('place') == packages[1]
                assert server.add_package(PLACE, 'package') == packages[2]
                assert server.add_package(USER1, 'package') == packages[3]
                person = server.register_person('user1', PASSWORD)
                assert person == packages[4]
                assert person.method_calls == [('register', (PASSWORD,))]
        assert_raises(AssertionError, server.add_package, None, 'package')

        self._test_server2(server.database)

    @mockified('pypsyc.server', ['Routing', 'Entity', 'Database', 'run_webif'])
    def _test_server2(self, database, Routing, Entity, Database, run_webif):
        routing = Routing.return_value
        root = Entity.return_value = StubEntity()
        Database.return_value = database
        executed = database.execute = AsyncMethod()

        server = Server(SERVER1, INTERFACE, 0, 0, ':memory:')
        assert routing.method_calls == [('init', (root,))]
        assert run_webif.call_args_list == []

        with mockified('pypsyc.server', ['iter_entry_points', 'Entity']) as x:
            with self._test_load_package(root, *x):
                executed.callback()
        assert_raises(AssertionError, server.add_package, None, 'package1')

    @contextmanager
    def _test_load_package(self, root, iter_entry_points, Entity):
        package_classes = [Mock(), Mock(), Mock(), Mock(), Mock()]
        entrypoint = Mock()
        entrypoint.load.side_effect = iter(package_classes).next
        iter_entry_points.return_value = [entrypoint]
        Entity.side_effect = StubEntity

        packages = [package.return_value for package in package_classes]
        yield packages
        assert Entity.call_args_list == [((root, PLACE),), ((root, USER1),)]
        assert iter_entry_points.call_args_list == [
                (('pypsyc.server.packages', 'package'),),
                (('pypsyc.server.packages', 'place'),),
                (('pypsyc.server.packages', 'package'),),
                (('pypsyc.server.packages', 'package'),),
                (('pypsyc.server.packages', 'person'),)]
        place = root.children[PLACE]
        person = root.children[USER1]
        assert package_classes[0].call_args_list == [((root,),)]
        assert package_classes[1].call_args_list == [((place,),)]
        assert package_classes[2].call_args_list == [((place,),)]
        assert package_classes[3].call_args_list == [((person,),)]
        assert package_classes[4].call_args_list == [((person,),)]
        assert root.packages == {'package': packages[0]}
        assert place.packages == {'place': packages[1], 'package': packages[2]}
        assert person.packages == {'package': packages[3],
                                   'person': packages[4]}

    @mockified('pypsyc.server', ['Routing', 'Entity', 'Database', 'signal',
                                 'reactor'])
    def test_shutdown(self, Routing, Entity, Database, signal, reactor):
        database = Database.return_value
        database.fetch.return_value = ()
        server = Server(SERVER1, INTERFACE, 0, 0, ':memory')
        database.reset_mock()

        server.shutdown()
        assert database.method_calls == [('stop',)]
        assert reactor.method_calls == [('stop',)]
