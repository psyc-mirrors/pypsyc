"""
    ::copyright: 2010 by Manuel Jacob
    :license: MIT
"""
from mock import Mock
from tests.constants import (SERVER1, SERVER2, SERVER1_UNI, SERVER2_UNI, USER1, USER2,
     USER1_UNI, USER2_UNI, RESOURCE, PLACE_UNI, VARIABLES)
from tests.helpers import mockified, rendered

from pypsyc.core.psyc import PSYCPacket
from pypsyc.server import Entity
from pypsyc.server.multicast import ContextSlave
from pypsyc.server.person import Resource


EXTERN_USER_UNI = SERVER2_UNI.chain(USER1)
SET_PACKET = PSYCPacket({'=': VARIABLES})
ADD_PACKET = PSYCPacket({'+': VARIABLES})
REMOVE_PACKET = PSYCPacket({'-': VARIABLES})

class TestContextMaster(object):
    def setup(self):
        self.server = Mock()
        self.server.hostname = SERVER1
        self.server.routing.mrouting_table = self.mrouting_table = {}
        root = Entity(server=self.server)
        self.user1 = Entity(root, USER1)
        self.user1.castmsg = Mock()
        user2 = Entity(root, USER2)
        Resource(user2, RESOURCE, Mock())

    def test_extern_member(self):
        circuit = Mock()
        self.server.routing.srouting_table = {SERVER2: circuit}

        self.user1.context_master.add_member(EXTERN_USER_UNI)
        assert self.mrouting_table[USER1_UNI] == set([circuit])

        self.user1.context_master.remove_member(EXTERN_USER_UNI)
        assert self.mrouting_table[USER1_UNI] == set()

    def test_set_persistent(self):
        self.user1.context_master.state_set(**VARIABLES)
        assert self.user1.castmsg.call_args_list == [((SET_PACKET,),),]

        state = self.user1.context_master.add_member(USER2_UNI)
        assert state == VARIABLES

    def test_persistent_list(self):
        self.user1.context_master.state_add(**VARIABLES)
        assert self.user1.castmsg.call_args_list == [((ADD_PACKET,),)]
        self.user1.castmsg.reset_mock()

        state = self.user1.context_master.add_member(USER2_UNI)
        assert state == VARIABLES

        self.user1.context_master.state_remove(**VARIABLES)
        assert self.user1.castmsg.call_args_list == [((REMOVE_PACKET,),)]

        state = self.user1.context_master.add_member(USER2_UNI)
        assert state == {}

    def test_remove_inheritance(self):
        self.user1.context_master.state_add(_a='a', _a_b='b')
        self.user1.context_master.state_remove(_a='a')

        state = self.user1.context_master.add_member(USER2_UNI)
        assert state == {}


RESOURCE1 = '*resource1'
RESOURCE2 = '*resource2'
USER1_RESOURCE1_UNI = USER1_UNI.chain(RESOURCE1)
USER1_RESOURCE2_UNI = USER1_UNI.chain(RESOURCE2)

class TestContextSlave(object):
    @mockified('pypsyc.server.multicast', ['ContextSlaveProtocol'])
    def setup(self, ContextSlaveProtocol):
        person = Mock()
        self.entity = person.entity
        self.entity._root.uni = SERVER1_UNI
        self.entity.uni = USER1_UNI
        self.entity.server.routing.mrouting_table = self.mrouting_table = {}
        self.entity.children = {}
        self.entity.children[RESOURCE1] = self.resource1 = Mock()
        self.entity.children[RESOURCE2] = self.resource2 = Mock()
        self.protocol = ContextSlaveProtocol.return_value
        self.protocol.enter.return_value = VARIABLES
        self.context_slave = ContextSlave(person)
        assert ContextSlaveProtocol.call_args_list == [((person,),)]

    def test_enter_leave(self):
        self.context_slave.enter(PLACE_UNI, USER1_RESOURCE1_UNI)
        assert self.protocol.method_calls == [('enter', (PLACE_UNI,))]
        assert self.mrouting_table[PLACE_UNI] == set([self.resource1.circuit])
        assert self.resource1.circuit.method_calls == [
                ('send', ({'_context': PLACE_UNI}, rendered(SET_PACKET)))]
        self.protocol.reset_mock()

        self.context_slave.leave(PLACE_UNI, USER1_RESOURCE1_UNI)
        assert self.protocol.method_calls == [('leave', (PLACE_UNI,))]
        assert self.mrouting_table[PLACE_UNI] == set()

    def test_enter_leave_all_resources(self):
        self.context_slave.enter(PLACE_UNI)
        assert self.protocol.method_calls == [('enter', (PLACE_UNI,))]
        assert self.mrouting_table[PLACE_UNI] == set([self.resource1.circuit,
                                                      self.resource2.circuit])
        assert self.resource1.circuit.method_calls == [
                ('send', ({'_context': PLACE_UNI}, rendered(SET_PACKET)),)]
        assert self.resource2.circuit.method_calls == [
                ('send', ({'_context': PLACE_UNI}, rendered(SET_PACKET)),)]
        self.protocol.reset_mock()

        self.context_slave.leave(PLACE_UNI)
        assert self.protocol.method_calls == [('leave', (PLACE_UNI,))]
        assert self.mrouting_table[PLACE_UNI] == set()

    def test_enter_leave_two_resources(self):
        extern_circuit = Mock()
        self.mrouting_table[PLACE_UNI] = set([extern_circuit])

        self.context_slave.enter(PLACE_UNI, USER1_RESOURCE1_UNI)
        self.context_slave.enter(PLACE_UNI, USER1_RESOURCE2_UNI)
        assert self.protocol.method_calls == [('enter', (PLACE_UNI,))]
        assert self.mrouting_table[PLACE_UNI] == set([extern_circuit,
                                                      self.resource1.circuit,
                                                      self.resource2.circuit])
        assert self.resource1.circuit.method_calls == [
                ('send', ({'_context': PLACE_UNI}, rendered(SET_PACKET)))]
        self.protocol.reset_mock()

        self.context_slave.leave(PLACE_UNI, USER1_RESOURCE1_UNI)
        assert self.protocol.method_calls == []
        self.context_slave.leave(PLACE_UNI, USER1_RESOURCE2_UNI)
        assert self.protocol.method_calls == [('leave', (PLACE_UNI,))]
        assert self.mrouting_table[PLACE_UNI] == set([extern_circuit])

    def test_enter_leave_extern(self):
        self.context_slave.enter(EXTERN_USER_UNI, USER1_RESOURCE1_UNI)
        assert self.protocol.method_calls == [('enter', (EXTERN_USER_UNI,))]
        assert self.mrouting_table[EXTERN_USER_UNI] == \
                set([self.resource1.circuit])
        assert self.resource1.circuit.method_calls == [
                ('send', ({'_context':EXTERN_USER_UNI}, rendered(SET_PACKET)))]
        self.protocol.reset_mock()

        self.context_slave.leave(EXTERN_USER_UNI, USER1_RESOURCE1_UNI)
        assert self.protocol.method_calls == [('leave', (EXTERN_USER_UNI,))]
        assert EXTERN_USER_UNI not in self.mrouting_table

    def test_leave_all(self):
        self.context_slave.enter(PLACE_UNI, USER1_RESOURCE1_UNI)
        self.protocol.reset_mock()

        self.context_slave.leave_all()
        assert self.protocol.method_calls == [('leave', (PLACE_UNI,))]
        assert self.mrouting_table[PLACE_UNI] == set()

    def test_not_leave(self):
        self.context_slave.leave(PLACE_UNI)
        assert self.protocol.method_calls == []
