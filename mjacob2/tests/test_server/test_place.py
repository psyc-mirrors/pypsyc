"""
    :copyright: 2010 by Manuel Jacob
    :license: MIT
"""
from mock import Mock
from tests.constants import (SERVER1, USER1_UNI, USER2_UNI, USER1_NICK,
     RESOURCE1_UNI, RESOURCE2_UNI, PLACE, VARIABLES, MESSAGE)
from tests.helpers import mockified

from pypsyc.server import Entity
from pypsyc.server.place import Place


class TestPlace(object):
    def setup(self):
        self.server = Mock()
        self.server.hostname = SERVER1

        root = Entity(server=self.server)
        self.entity = Entity(root, PLACE)
        self.entity.context_master = Mock()

    @mockified('pypsyc.server.place', ['ContextProtocol',
                                       'ConferencingProtocol'])
    def test_subscription(self, ContextProtocol, ConferencingProtocol):
        conferencing_protocol = ConferencingProtocol.return_value
        self.entity.context_master.add_member.return_value = VARIABLES

        place = Place(self.entity)
        assert ContextProtocol.call_args_list == [((place,),)]

        state = place.enter_request(USER1_UNI)
        assert state == VARIABLES
        assert conferencing_protocol.method_calls == [
                ('cast_member_entered', (USER1_UNI, USER1_NICK))]
        assert self.entity.context_master.method_calls == [
                ('add_member', (USER1_UNI,))]

    @mockified('pypsyc.server.place', ['ConferencingProtocol'])
    def test_leave(self, ConferencingProtocol):
        conferencing_protocol = ConferencingProtocol.return_value
        place = Place(self.entity)

        place.enter_request(USER1_UNI)
        self.entity.context_master.reset_mock()
        conferencing_protocol.reset_mock()

        place.leave_context(USER1_UNI)
        assert self.entity.context_master.method_calls == [
                ('remove_member', (USER1_UNI,))]
        assert conferencing_protocol.method_calls == [
                ('cast_member_left', (USER1_UNI,))]

    @mockified('pypsyc.server.place', ['ConferencingProtocol'])
    def test_public_message(self, ConferencingProtocol):
        conferencing_protocol = ConferencingProtocol.return_value

        place = Place(self.entity)
        assert ConferencingProtocol.call_args_list == [((place,),)]
        place.enter_request(USER1_UNI)
        place.enter_request(USER2_UNI)
        place.leave_context(USER2_UNI)
        conferencing_protocol.reset_mock()

        place.public_message(RESOURCE2_UNI, MESSAGE)
        place.public_message(RESOURCE1_UNI, MESSAGE)
        assert conferencing_protocol.method_calls == [
                ('cast_public_message', (USER1_UNI, MESSAGE))]
