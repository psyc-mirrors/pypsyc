"""
    :copyright: 2010 by Manuel Jacob
    :license: MIT
"""
from mock import Mock, sentinel
from nose.tools import assert_raises
from tests.constants import USER1, USER1_NICK, PASSWORD
from tests.helpers import mockified, PlaceHolder

from pypsyc.server import webif
from pypsyc.server.db import Database
from pypsyc.server.webif import app, PersistentStore, run_webif


class TestViews(object):
    @classmethod
    def setup_class(cls):
        app.secret_key = 'testing key'

    def setup(self):
        self.server = webif.server = Mock()
        self.server.root.children = {}
        self.client = app.test_client()

    def test_index(self):
        rv = self.client.get('/')
        assert 'register' in rv.data

    def _register(self, username, password, password2):
        return self.client.post('/register', data=dict(
                username=username, password=password, password2=password2
        ), follow_redirects=True)

    def test_register_get(self):
        rv = self.client.get('/register')
        assert 'Username' in rv.data
        assert 'Register' in rv.data

    def test_register_post(self):
        rv = self._register(USER1_NICK, PASSWORD, PASSWORD)
        assert 'you were registered' in rv.data
        assert self.server.method_calls == [
                ('register_person', (USER1_NICK, PASSWORD))]

    def test_register_nousername(self):
        rv = self._register('', '', '')
        assert 'please specify a valid username' in rv.data

    def test_register_username_inuse(self):
        self.server.root.children = {USER1: True}

        rv = self._register(USER1_NICK, '', '')
        assert 'username already in use' in rv.data

    def test_register_nopassword(self):
        rv = self._register(USER1_NICK, '', '')
        assert 'your password must have at least 6 characters' in rv.data

    def test_register_shortpassword(self):
        rv = self._register(USER1_NICK, 'passw', 'passw')
        assert 'your password must have at least 6 characters' in rv.data

    def test_register_unmatching_passwords(self):
        rv = self._register(USER1_NICK, PASSWORD, 'password2')
        assert 'the two passwords do not match' in rv.data


def test_persistent_store():
    webif.server = Mock()
    webif.server.database = Database(':memory:')

    store = PersistentStore()
    assert_raises(KeyError, store.__getitem__, 'key')
    assert dict(store) == {}
    assert len(store) == 0

    store['key'] = 'value'
    assert store['key'] == 'value'
    assert len(store) == 1

    store['key'] = 'value2'
    assert dict(store) == {'key': 'value2'}
    assert len(store) == 1

    del store['key']
    assert dict(store) == {}
    assert len(store) == 0


@mockified('pypsyc.server.webif', ['PersistentStore', 'urandom',
                                   'WSGIResource', 'Site', 'reactor',
                                   'greenlet'])
def test_run_webif_ssl(PersistentStore, urandom, WSGIResource, Site, reactor,
                       greenlet):
    store = PersistentStore.return_value = {'secret_key': 'key1'*5}
    reactor_ph = PlaceHolder()
    threadpool_ph = PlaceHolder()
    wsgi_resource = WSGIResource.return_value
    site = Site.return_value

    run_webif(sentinel.server, sentinel.interface, sentinel.port,
              sentinel.context_factory)
    assert webif.server == sentinel.server
    assert PersistentStore.call_args_list == [()]
    assert webif.store == store
    assert app.secret_key == 'key1'*5
    assert not urandom.called
    assert WSGIResource.call_args_list == [
            ((reactor_ph, threadpool_ph, app.wsgi_app),)]
    assert Site.call_args_list == [((wsgi_resource,),)]
    assert reactor.method_calls == [
            ('listenSSL', (sentinel.port, site, sentinel.context_factory),
             {'interface': sentinel.interface})]

    func = Mock()
    reactor_ph.obj.callFromThread(func, sentinel.a, b=sentinel.b)
    assert func.call_args_list == [((sentinel.a,), {'b': sentinel.b})]

    threadpool_ph.obj.callInThread(sentinel.func, sentinel.a, b=sentinel.b)
    assert greenlet.call_args_list == [((sentinel.func,),)]
    assert greenlet.return_value.method_calls == [
            ('switch', (sentinel.a,), {'b': sentinel.b})]


@mockified('pypsyc.server.webif', ['PersistentStore', 'urandom',
                                   'WSGIResource', 'Site', 'reactor'])
def test_run_webif_tcp(PersistentStore, urandom, WSGIResource, Site, reactor):
    store = PersistentStore.return_value = {}
    rand = urandom.return_value = 'key2'*5
    wsgi_resource = WSGIResource.return_value
    site = Site.return_value

    run_webif(sentinel.server, sentinel.interface, sentinel.port, None)
    assert urandom.call_args_list == [((20,),)]
    assert app.secret_key == rand
    assert store['secret_key'] == rand
    assert Site.call_args_list == [((wsgi_resource,),)]
    assert reactor.method_calls == [
            ('listenTCP', (sentinel.port, site), {'interface': 'localhost'})]
