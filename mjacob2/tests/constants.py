from pypsyc.client.model import Presence
from pypsyc.core.mmp import Uni


SERVER1 = 'server1'
SERVER1_UNI = Uni('psyc://%s/' % SERVER1)
USER1 = '~user1'
USER2 = '~user2'
USER1_UNI = SERVER1_UNI.chain(USER1)
USER2_UNI = SERVER1_UNI.chain(USER2)
USER1_NICK = 'user1'
USER2_NICK = 'user2'
RESOURCE = '*Resource'
RESOURCE1_UNI = USER1_UNI.chain(RESOURCE)
RESOURCE2_UNI = USER2_UNI.chain(RESOURCE)
PLACE = '@place'
PLACE_UNI = SERVER1_UNI.chain(PLACE)
SERVER2 = 'server2'
SERVER2_UNI = Uni('psyc://%s/' % SERVER2)
SERVER3 = 'server3'
SERVER3_UNI = Uni('psyc://%s/' % SERVER3)

INTERFACE = 'interface'
IP = '10.0.0.1'
PORT = 4404
VARIABLES = {'_key': 'value'}
CONTENT = ['']

PASSWORD = 'password'
MESSAGE = 'message'
UNKNOWN = 0
OFFLINE = 1
ONLINE = 7
PRESENCE_UNKNOWN = Presence(UNKNOWN)
PRESENCE_OFFLINE = Presence(OFFLINE)
PRESENCE_ONLINE = Presence(ONLINE)
PENDING = 'pending'
OFFERED = 'offered'
ESTABLISHED = 'established'
ERROR = 'error'
EXCEPTION = Exception(ERROR)
