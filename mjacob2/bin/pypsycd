#!/usr/bin/env python
"""
    :copyright: 2010 by Manuel Jacob
    :license: MIT
"""
import logging
logging.basicConfig()
from optparse import OptionParser
from os import path, mkdir
from socket import gethostname

from twisted.internet import reactor

from pypsyc.server import Server


def main():
    parser = OptionParser()
    parser.add_option('-d', '--directory', dest='directory',
                      default=path.expanduser(path.join('~', '.pypsycd')),
                      help="directory where the database is stored "
                           "(defaults to ~/.pypsycd)")
    parser.add_option('-H', '--hostname', dest='hostname',
                      help="hostname of the psyc server")
    parser.add_option('-i', '--interface', dest='interface', default='',
                      help="interface to bind to (defaults to all)")
    parser.add_option('-p', '--psyc-port', type='int', default=4404,
                      help="port to listen for incoming psyc connections "
                           "or 0 to disable them (defaults to 4404)")
    parser.add_option('-w', '--webif-port', type='int', default=8080,
                      help="port of the web interface or 0 to disable it "
                           "(defaults to 8080)")
    options = parser.parse_args()[0]

    if not path.exists(options.directory):
        mkdir(options.directory)
    Server(hostname=options.hostname or gethostname(),
           interface=options.interface,
           psyc_port=options.psyc_port,
           webif_port=options.webif_port,
           db_file=path.join(options.directory, 'database'))
    reactor.run()

if __name__ == '__main__':
    main()
